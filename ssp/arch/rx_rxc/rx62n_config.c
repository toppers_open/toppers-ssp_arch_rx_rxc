/*
 *  TOPPERS/SSP Kernel
 *      Smallest Set Profile Kernel
 * 
 *  Copyright (C) 2010 by Witz Corporation, JAPAN
 * 
 *  上記著作権者は，以下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェ
 *  ア（本ソフトウェアを改変したものを含む．以下同じ）を使用・複製・改
 *  変・再配布（以下，利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを，ライブラリ形式など，他のソフトウェア開発に使
 *      用できる形で再配布する場合には，再配布に伴うドキュメント（利用
 *      者マニュアルなど）に，上記の著作権表示，この利用条件および下記
 *      の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを，機器に組み込むなど，他のソフトウェア開発に使
 *      用できない形で再配布する場合には，次のいずれかの条件を満たすこ
 *      と．
 *    (a) 再配布に伴うドキュメント（利用者マニュアルなど）に，上記の著
 *        作権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 再配布の形態を，別に定める方法によって，TOPPERSプロジェクトに
 *        報告すること．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者およびTOPPERSプロジェクトを免責すること．
 *      また，本ソフトウェアのユーザまたはエンドユーザからのいかなる理
 *      由に基づく請求からも，上記著作権者およびTOPPERSプロジェクトを
 *      免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者お
 *  よびTOPPERSプロジェクトは，本ソフトウェアに関して，特定の使用目的
 *  に対する適合性も含めて，いかなる保証も行わない．また，本ソフトウェ
 *  アの利用により直接的または間接的に生じたいかなる損害に関しても，そ
 *  の責任を負わない．
 * 
 */

/*
 *		プロセッサ依存モジュール（RX62N用）
 */

#include "kernel_impl.h"


/*
 *  割込み要因プライオリティレジスタアドレステーブル
 */
volatile uint8_t __evenaccess * const ipr_reg_addr[ INHNO_MAX ] = {
	NULL, NULL, NULL, NULL, NULL,	/* No.0 - 4   予約 */
	NULL, NULL, NULL, NULL, NULL,	/* No.5 - 9   予約 */
	NULL, NULL, NULL, NULL, NULL,	/* No.10 - 14 予約 */
	NULL,							/* No.15  予約 */
	ICU_IPR00_ADDR,					/* No.16  バスエラー BUSERR */
	NULL, NULL, NULL, NULL,			/* No.17 - 20 予約 */
	ICU_IPR01_ADDR,					/* No.21  FCU FIFERR */
	NULL,							/* No.22  予約 */
	ICU_IPR02_ADDR,					/* No.23  FCU FRDYI */
	NULL, NULL, NULL, 				/* No.24 -26  予約 */
	ICU_IPR03_ADDR,					/* No.27  SWINT */
	ICU_IPR04_ADDR,					/* No.28  CMTユニット0 CMT0 */
	ICU_IPR05_ADDR,					/* No.29  CMTユニット0 CMT1 */
	ICU_IPR06_ADDR,					/* No.30  CMTユニット1 CMT2 */
	ICU_IPR07_ADDR,					/* No.31  CMTユニット1 CMT3 */
	ICU_IPR08_ADDR, 				/* No.32  ETHER */
	NULL, NULL, NULL,				/* No.33 - 35 予約 */
	ICU_IPR0C_ADDR,				 	/* No.36  USB0 D0FIFO0 */
	ICU_IPR0D_ADDR,				 	/* No.37  USB0 D1FIFO0 */
	ICU_IPR0E_ADDR,				 	/* No.38  USB0 USBI0 */
	NULL,							/* No.39  予約 */
	ICU_IPR10_ADDR,				 	/* No.40  USB1 D0FIFO1 */
	ICU_IPR11_ADDR,				 	/* No.41  USB1 D1FIFO1 */
	ICU_IPR12_ADDR,				 	/* No.42  USB1 USBI1 */
	NULL,							/* No.43  予約 */
	ICU_IPR14_ADDR,				 	/* No.44  SPEI0 */
	ICU_IPR14_ADDR,				 	/* No.45  SPRI0 */
	ICU_IPR14_ADDR,				 	/* No.46  SPTI0 */
	ICU_IPR14_ADDR,				 	/* No.47  SPII0 */
	ICU_IPR15_ADDR,				 	/* No.48  SPEI0 */
	ICU_IPR15_ADDR,				 	/* No.49  SPRI0 */
	ICU_IPR15_ADDR,				 	/* No.50  SPTI0 */
	ICU_IPR15_ADDR,				 	/* No.51  SPII0 */
	NULL, NULL, NULL, NULL, 		/* No.52 - 55 予約 */
	ICU_IPR18_ADDR, 				/* No.56  CAN ERS0 */
	ICU_IPR18_ADDR, 				/* No.57  CAN RXF0 */
	ICU_IPR18_ADDR, 				/* No.58  CAN TXF0 */
	ICU_IPR18_ADDR, 				/* No.59  CAN RXM0 */
	ICU_IPR18_ADDR, 				/* No.60  CAN TXM0 */
	NULL,							/* No.61  予約 */
	ICU_IPR1D_ADDR,				 	/* No.62  RTC PRD */
	ICU_IPR1E_ADDR,				 	/* No.63  RTC CUP */
	ICU_IPR20_ADDR,					/* No.64  外部端子 IRQ0 */
	ICU_IPR21_ADDR,					/* No.65  外部端子 IRQ1 */
	ICU_IPR22_ADDR,					/* No.66  外部端子 IRQ2 */
	ICU_IPR23_ADDR,					/* No.67  外部端子 IRQ3 */
	ICU_IPR24_ADDR,					/* No.68  外部端子 IRQ4 */
	ICU_IPR25_ADDR,					/* No.69  外部端子 IRQ5 */
	ICU_IPR26_ADDR,					/* No.70  外部端子 IRQ6 */
	ICU_IPR27_ADDR,					/* No.71  外部端子 IRQ7 */
	ICU_IPR28_ADDR,					/* No.72  外部端子 IRQ8 */
	ICU_IPR29_ADDR,					/* No.73  外部端子 IRQ9 */
	ICU_IPR2A_ADDR,					/* No.74  外部端子 IRQ10 */
	ICU_IPR2B_ADDR,					/* No.75  外部端子 IRQ11 */
	ICU_IPR2C_ADDR,					/* No.76  外部端子 IRQ12 */
	ICU_IPR2D_ADDR,					/* No.77  外部端子 IRQ13 */
	ICU_IPR2E_ADDR,					/* No.78  外部端子 IRQ14 */
	ICU_IPR2F_ADDR,					/* No.79  外部端子 IRQ15 */
	NULL, NULL, NULL, NULL, NULL,	/* No.80 - 84 予約 */
	NULL, NULL, NULL, NULL, NULL,	/* No.85 - 89 予約 */
	ICU_IPR3A_ADDR, 				/* No.90  USBR0 */
	ICU_IPR3B_ADDR, 				/* No.91  USBR1 */
	ICU_IPR3B_ADDR,					/* No.92  RTC ALM */
	NULL, NULL,						/* No.93 - 94 予約 */
	NULL,							/* No.95  予約 */
	ICU_IPR40_ADDR,					/* No.96  WDT WOVI */
	NULL,							/* No.97  予約 */
	ICU_IPR44_ADDR,					/* No.98  AD0 ADI0 */
	ICU_IPR45_ADDR,					/* No.99  AD1 ADI1 */
	NULL, NULL, 					/* No.100 - 101 予約 */
	ICU_IPR48_ADDR,					/* No.102 S12AD0 */
	NULL, NULL,						/* No.103 - 104 予約 */
	NULL, NULL, NULL, NULL, NULL,	/* No.105 - 109 予約 */
	NULL, NULL, NULL, NULL,			/* No.110 - 113 予約 */
	ICU_IPR51_ADDR,					/* No.114 MTU0 TGIA0 */
	ICU_IPR51_ADDR,					/* No.115 MTU0 TGIB0 */
	ICU_IPR51_ADDR,					/* No.116 MTU0 TGIC0 */
	ICU_IPR51_ADDR,					/* No.117 MTU0 TGID0 */
	ICU_IPR52_ADDR,					/* No.118 MTU0 TCIV0 */
	ICU_IPR52_ADDR,					/* No.119 MTU0 TCIE0 */
	ICU_IPR52_ADDR,					/* No.120 MTU0 TCIF0 */
	ICU_IPR53_ADDR,					/* No.121 MTU1 TGIA1 */
	ICU_IPR53_ADDR,					/* No.122 MTU1 TGIB1 */
	ICU_IPR54_ADDR,					/* No.123 MTU1 TCIV1 */
	ICU_IPR54_ADDR,					/* No.124 MTU1 TCIU1 */
	ICU_IPR55_ADDR,					/* No.125 MTU2 TGIA2 */
	ICU_IPR55_ADDR,					/* No.126 MTU2 TGIB2 */
	ICU_IPR56_ADDR,					/* No.127 MTU2 TCIV2 */
	ICU_IPR56_ADDR,					/* No.128 MTU2 TCIU2 */
	ICU_IPR57_ADDR,					/* No.129 MTU3 TGIA3 */
	ICU_IPR57_ADDR,					/* No.130 MTU3 TGIB3 */
	ICU_IPR57_ADDR,					/* No.131 MTU3 TCIC3 */
	ICU_IPR57_ADDR,					/* No.132 MTU3 TCID3 */
	ICU_IPR58_ADDR,					/* No.133 MTU3 TCIV3 */
	ICU_IPR59_ADDR,					/* No.134 MTU4 TGIA4 */
	ICU_IPR59_ADDR,					/* No.135 MTU4 TGIB4 */
	ICU_IPR59_ADDR,					/* No.136 MTU4 TCIC4 */
	ICU_IPR59_ADDR,					/* No.137 MTU4 TCID4 */
	ICU_IPR5A_ADDR,					/* No.138 MTU4 TCIV4 */
	ICU_IPR5B_ADDR,					/* No.139 MTU5 TCIU5 */
	ICU_IPR5B_ADDR,					/* No.140 MTU5 TCIV5 */
	ICU_IPR5B_ADDR,					/* No.141 MTU5 TCIW5 */
	ICU_IPR5C_ADDR,					/* No.142 MTU6 TGIA6 */
	ICU_IPR5C_ADDR,					/* No.143 MTU6 TGIB6 */
	ICU_IPR5C_ADDR,					/* No.144 MTU6 TGIC6 */
	ICU_IPR5C_ADDR,					/* No.145 MTU6 TGID6 */
	ICU_IPR5D_ADDR,					/* No.146 MTU6 TCIV6 */
	ICU_IPR5D_ADDR,					/* No.147 MTU6 TCIE6 */
	ICU_IPR5D_ADDR,					/* No.148 MTU6 TCIF6 */
	ICU_IPR5E_ADDR,					/* No.149 MTU7 TGIA7 */
	ICU_IPR5E_ADDR,					/* No.150 MTU7 TGIB7 */
	ICU_IPR5F_ADDR,					/* No.151 MTU7 TCIV7 */
	ICU_IPR5F_ADDR,					/* No.152 MTU7 TCIU7 */
	ICU_IPR60_ADDR,					/* No.153 MTU8 TGIA8 */
	ICU_IPR60_ADDR,					/* No.154 MTU8 TGIB8 */
	ICU_IPR61_ADDR,					/* No.155 MTU8 TCIV8 */
	ICU_IPR61_ADDR,					/* No.156 MTU8 TCIU8 */
	ICU_IPR62_ADDR,					/* No.157 MTU9 TGIA9 */
	ICU_IPR62_ADDR,					/* No.158 MTU9 TGIB9 */
	ICU_IPR62_ADDR,					/* No.159 MTU9 TGIC9 */
	ICU_IPR62_ADDR,					/* No.160 MTU9 TGID9 */
	ICU_IPR63_ADDR,					/* No.161 MTU9 TCIV9 */
	ICU_IPR64_ADDR,					/* No.162 MTU10 TGIA10 */
	ICU_IPR64_ADDR,					/* No.163 MTU10 TGIB10 */
	ICU_IPR64_ADDR,					/* No.164 MTU10 TGIC10 */
	ICU_IPR64_ADDR,					/* No.165 MTU10 TGID10 */
	ICU_IPR65_ADDR,					/* No.166 MTU10 TCIV10 */
	ICU_IPR66_ADDR,					/* No.167 MTU11 TGIU11 */
	ICU_IPR66_ADDR,					/* No.168 MTU11 TGIV11 */
	ICU_IPR66_ADDR,					/* No.169 MTU11 TCIW11 */
	ICU_IPR67_ADDR,					/* No.170 POE OEI1 */
	ICU_IPR67_ADDR,					/* No.171 POE OEI2 */
	ICU_IPR67_ADDR,					/* No.172 POE OEI3 */
	ICU_IPR67_ADDR,					/* No.173 POE OEI4 */
	ICU_IPR68_ADDR,					/* No.174 TMR0 CMIA0 */
	ICU_IPR68_ADDR,					/* No.175 TMR0 CMIB0 */
	ICU_IPR68_ADDR,					/* No.176 TMR0 OVI0 */
	ICU_IPR69_ADDR,					/* No.177 TMR1 CMIA1 */
	ICU_IPR69_ADDR,					/* No.178 TMR1 CMIB1 */
	ICU_IPR69_ADDR,					/* No.179 TMR1 OVI1 */
	ICU_IPR6A_ADDR,					/* No.180 TMR2 CMIA2 */
	ICU_IPR6A_ADDR,					/* No.181 TMR2 CMIB2 */
	ICU_IPR6A_ADDR,					/* No.182 TMR2 OVI2 */
	ICU_IPR6B_ADDR,					/* No.183 TMR3 CMIA3 */
	ICU_IPR6B_ADDR,					/* No.184 TMR3 CMIB3 */
	ICU_IPR6B_ADDR,					/* No.185 TMR3 OVI3 */
	NULL, NULL, NULL, NULL, NULL,	/* No.186 - 190 予約 */
	NULL, NULL, NULL, NULL, NULL,	/* No.191 - 195 予約 */
	NULL, NULL,						/* No.196 - 197 予約 */
	ICU_IPR70_ADDR,					/* No.198 DMAC DMTEND0 */
	ICU_IPR71_ADDR,					/* No.199 DMAC DMTEND1 */
	ICU_IPR72_ADDR,					/* No.200 DMAC DMTEND2 */
	ICU_IPR73_ADDR,					/* No.201 DMAC DMTEND3 */
	ICU_IPR73_ADDR,					/* No.202 EXDMAC EXDMACI0 */
	ICU_IPR73_ADDR,					/* No.203 EXDMAC EXDMACI1 */
	NULL, NULL, NULL,				/* No.204 - 206 予約 */
	NULL, NULL, NULL, NULL, NULL,	/* No.207 - 211 予約 */
	NULL, NULL,						/* No.212 - 213 予約 */
	ICU_IPR80_ADDR,					/* No.214 SCI0 ERI0 */
	ICU_IPR80_ADDR,					/* No.215 SCI0 RXI0 */
	ICU_IPR80_ADDR,					/* No.216 SCI0 TXI0 */
	ICU_IPR80_ADDR,					/* No.217 SCI0 TEI0 */
	ICU_IPR81_ADDR,					/* No.218 SCI1 ERI1 */
	ICU_IPR81_ADDR,					/* No.219 SCI1 RXI1 */
	ICU_IPR81_ADDR,					/* No.220 SCI1 TXI1 */
	ICU_IPR81_ADDR,					/* No.221 SCI1 TEI1 */
	ICU_IPR82_ADDR,					/* No.222 SCI2 ERI2 */
	ICU_IPR82_ADDR,					/* No.223 SCI2 RXI2 */
	ICU_IPR82_ADDR,					/* No.224 SCI2 TXI2 */
	ICU_IPR82_ADDR,					/* No.225 SCI2 TEI2 */
	ICU_IPR83_ADDR,					/* No.226 SCI3 ERI3 */
	ICU_IPR83_ADDR,					/* No.227 SCI3 RXI3 */
	ICU_IPR83_ADDR,					/* No.228 SCI3 TXI3 */
	ICU_IPR83_ADDR,					/* No.229 SCI3 TEI3 */
	ICU_IPR84_ADDR,					/* No.230 SCI4 ERI4 */
	ICU_IPR84_ADDR,					/* No.231 SCI4 RXI4 */
	ICU_IPR84_ADDR,					/* No.232 SCI4 TXI4 */
	ICU_IPR84_ADDR,					/* No.233 SCI4 TEI4 */
	ICU_IPR85_ADDR,					/* No.234 SCI5 ERI5 */
	ICU_IPR85_ADDR,					/* No.235 SCI5 RXI5 */
	ICU_IPR85_ADDR,					/* No.236 SCI5 TXI5 */
	ICU_IPR85_ADDR,					/* No.237 SCI5 TEI5 */
	ICU_IPR86_ADDR,					/* No.238 SCI6 ERI6 */
	ICU_IPR86_ADDR,					/* No.239 SCI6 RXI6 */
	ICU_IPR86_ADDR,					/* No.240 SCI6 TXI6 */
	ICU_IPR86_ADDR,					/* No.241 SCI6 TEI6 */
	NULL, NULL, NULL, NULL,			/* No.242 - 245 予約 */
	ICU_IPR88_ADDR,					/* No.246 RIIC0 ICEEI0 */
	ICU_IPR89_ADDR,					/* No.247 RIIC0 ICRXI0 */
	ICU_IPR8A_ADDR,					/* No.248 RIIC0 ICTXI0 */
	ICU_IPR8B_ADDR,					/* No.249 RIIC0 ICTEI0 */
	ICU_IPR8C_ADDR,					/* No.250 RIIC1 ICEEI1 */
	ICU_IPR8D_ADDR,					/* No.251 RIIC1 ICRXI1 */
	ICU_IPR8E_ADDR,					/* No.252 RIIC1 ICTXI1 */
	ICU_IPR8F_ADDR,					/* No.253 RIIC1 ICTEI1 */
	NULL, NULL,						/* No.254 - 255 予約 */
};


/*
 *  割込み要求許可レジスタアドレステーブル
 */
const IER_INFO ier_reg_addr[ INHNO_MAX ] = {
	{ NULL, INVALID_OFFSET },			/* No.0   予約 */
	{ NULL, INVALID_OFFSET },			/* No.1   予約 */
	{ NULL, INVALID_OFFSET },			/* No.2   予約 */
	{ NULL, INVALID_OFFSET },			/* No.3   予約 */
	{ NULL, INVALID_OFFSET },			/* No.4   予約 */
	{ NULL, INVALID_OFFSET },			/* No.5   予約 */
	{ NULL, INVALID_OFFSET },			/* No.6   予約 */
	{ NULL, INVALID_OFFSET },			/* No.7   予約 */
	{ NULL, INVALID_OFFSET },			/* No.8   予約 */
	{ NULL, INVALID_OFFSET },			/* No.9   予約 */
	{ NULL, INVALID_OFFSET },			/* No.10  予約 */
	{ NULL, INVALID_OFFSET },			/* No.11  予約 */
	{ NULL, INVALID_OFFSET },			/* No.12  予約 */
	{ NULL, INVALID_OFFSET },			/* No.13  予約 */
	{ NULL, INVALID_OFFSET },			/* No.14  予約 */
	{ NULL, INVALID_OFFSET },			/* No.15  予約 */
	{ ICU_IER02_ADDR, ICU_IEN0_BIT },	/* No.16  バスエラー BUSERR */
	{ NULL, INVALID_OFFSET },			/* No.17  予約 */
	{ NULL, INVALID_OFFSET },			/* No.18  予約 */
	{ NULL, INVALID_OFFSET },			/* No.19  予約 */
	{ NULL, INVALID_OFFSET },			/* No.20  予約 */
	{ ICU_IER02_ADDR, ICU_IEN5_BIT },	/* No.21  FCU FIFERR */
	{ NULL, INVALID_OFFSET },			/* No.22  予約 */
	{ ICU_IER02_ADDR, ICU_IEN7_BIT },	/* No.23  FCU FRDYI */
	{ NULL, INVALID_OFFSET },			/* No.24  予約 */
	{ NULL, INVALID_OFFSET },			/* No.25  予約 */
	{ NULL, INVALID_OFFSET },			/* No.26  予約 */
	{ ICU_IER03_ADDR, ICU_IEN3_BIT },	/* No.27  予約 */
	{ ICU_IER03_ADDR, ICU_IEN4_BIT },	/* No.28  CMTユニット0 CMT0 */
	{ ICU_IER03_ADDR, ICU_IEN5_BIT },	/* No.29  CMTユニット0 CMT1 */
	{ ICU_IER03_ADDR, ICU_IEN6_BIT },	/* No.30  CMTユニット1 CMT2 */
	{ ICU_IER03_ADDR, ICU_IEN7_BIT },	/* No.31  CMTユニット1 CMT3 */
	{ ICU_IER04_ADDR, ICU_IEN0_BIT },	/* No.32  ETHER */
	{ NULL, INVALID_OFFSET },			/* No.33  予約 */
	{ NULL, INVALID_OFFSET },			/* No.34  予約 */
	{ NULL, INVALID_OFFSET },			/* No.35  予約 */
	{ ICU_IER04_ADDR, ICU_IEN3_BIT },	/* No.36  USB0 D0FIFO0 */
	{ ICU_IER04_ADDR, ICU_IEN4_BIT },	/* No.37  USB0 D1FIFO0 */
	{ ICU_IER04_ADDR, ICU_IEN5_BIT },	/* No.38  USB0 USBI0 */
	{ NULL, INVALID_OFFSET },			/* No.39  予約 */
	{ ICU_IER05_ADDR, ICU_IEN0_BIT },	/* No.40  USB1 D0FIFO1 */
	{ ICU_IER05_ADDR, ICU_IEN1_BIT },	/* No.41  USB1 D1FIFO1 */
	{ ICU_IER05_ADDR, ICU_IEN2_BIT },	/* No.42  USB1 USBI1 */
	{ NULL, INVALID_OFFSET },			/* No.43  予約 */
	{ ICU_IER05_ADDR, ICU_IEN4_BIT },	/* No.44  SPEI0 */
	{ ICU_IER05_ADDR, ICU_IEN5_BIT },	/* No.45  SPRI0 */
	{ ICU_IER05_ADDR, ICU_IEN6_BIT },	/* No.46  SPTI0 */
	{ ICU_IER05_ADDR, ICU_IEN7_BIT },	/* No.47  SPII0 */
	{ ICU_IER06_ADDR, ICU_IEN0_BIT },	/* No.48  SPEI0 */
	{ ICU_IER06_ADDR, ICU_IEN1_BIT },	/* No.49  SPRI0 */
	{ ICU_IER06_ADDR, ICU_IEN2_BIT },	/* No.50  SPTI0 */
	{ ICU_IER06_ADDR, ICU_IEN3_BIT },	/* No.51  SPII0 */
	{ NULL, INVALID_OFFSET },			/* No.52  予約 */
	{ NULL, INVALID_OFFSET },			/* No.53  予約 */
	{ NULL, INVALID_OFFSET },			/* No.54  予約 */
	{ NULL, INVALID_OFFSET },			/* No.55  予約 */
	{ ICU_IER07_ADDR, ICU_IEN0_BIT },	/* No.56  CAN ERS0 */
	{ ICU_IER07_ADDR, ICU_IEN1_BIT },	/* No.57  CAN RXF0 */
	{ ICU_IER07_ADDR, ICU_IEN2_BIT },	/* No.58  CAN TXF0 */
	{ ICU_IER07_ADDR, ICU_IEN3_BIT },	/* No.59  CAN RXM0 */
	{ ICU_IER07_ADDR, ICU_IEN4_BIT },	/* No.60  CAN TXM0 */
	{ NULL, INVALID_OFFSET },			/* No.61  予約 */
	{ ICU_IER07_ADDR, ICU_IEN6_BIT },	/* No.62  RTC PRD */
	{ ICU_IER07_ADDR, ICU_IEN7_BIT },	/* No.63  RTC CUP */
	{ ICU_IER08_ADDR, ICU_IEN0_BIT },	/* No.64  外部端子 IRQ0 */
	{ ICU_IER08_ADDR, ICU_IEN1_BIT },	/* No.65  外部端子 IRQ1 */
	{ ICU_IER08_ADDR, ICU_IEN2_BIT },	/* No.66  外部端子 IRQ2 */
	{ ICU_IER08_ADDR, ICU_IEN3_BIT },	/* No.67  外部端子 IRQ3 */
	{ ICU_IER08_ADDR, ICU_IEN4_BIT },	/* No.68  外部端子 IRQ4 */
	{ ICU_IER08_ADDR, ICU_IEN5_BIT },	/* No.69  外部端子 IRQ5 */
	{ ICU_IER08_ADDR, ICU_IEN6_BIT },	/* No.70  外部端子 IRQ6 */
	{ ICU_IER08_ADDR, ICU_IEN7_BIT },	/* No.71  外部端子 IRQ7 */
	{ ICU_IER09_ADDR, ICU_IEN0_BIT },	/* No.72  外部端子 IRQ8 */
	{ ICU_IER09_ADDR, ICU_IEN1_BIT },	/* No.73  外部端子 IRQ9 */
	{ ICU_IER09_ADDR, ICU_IEN2_BIT },	/* No.74  外部端子 IRQ10 */
	{ ICU_IER09_ADDR, ICU_IEN3_BIT },	/* No.75  外部端子 IRQ11 */
	{ ICU_IER09_ADDR, ICU_IEN4_BIT },	/* No.76  外部端子 IRQ12 */
	{ ICU_IER09_ADDR, ICU_IEN5_BIT },	/* No.77  外部端子 IRQ13 */
	{ ICU_IER09_ADDR, ICU_IEN6_BIT },	/* No.78  外部端子 IRQ14 */
	{ ICU_IER09_ADDR, ICU_IEN7_BIT },	/* No.79  外部端子 IRQ15 */
	{ NULL, INVALID_OFFSET },			/* No.80  予約 */
	{ NULL, INVALID_OFFSET },			/* No.81  予約 */
	{ NULL, INVALID_OFFSET },			/* No.82  予約 */
	{ NULL, INVALID_OFFSET },			/* No.83  予約 */
	{ NULL, INVALID_OFFSET },			/* No.84  予約 */
	{ NULL, INVALID_OFFSET },			/* No.85  予約 */
	{ NULL, INVALID_OFFSET },			/* No.86  予約 */
	{ NULL, INVALID_OFFSET },			/* No.87  予約 */
	{ NULL, INVALID_OFFSET },			/* No.88  予約 */
	{ NULL, INVALID_OFFSET },			/* No.89  予約 */
	{ ICU_IER0B_ADDR, ICU_IEN2_BIT },	/* No.90  USBR0 */
	{ ICU_IER0B_ADDR, ICU_IEN3_BIT },	/* No.91  USBR1 */
	{ ICU_IER0B_ADDR, ICU_IEN4_BIT },	/* No.92  RTC ALM */
	{ NULL, INVALID_OFFSET },			/* No.93  予約 */
	{ NULL, INVALID_OFFSET },			/* No.94  予約 */
	{ NULL, INVALID_OFFSET },			/* No.95  予約 */
	{ ICU_IER0C_ADDR, ICU_IEN0_BIT },	/* No.96  WDT WOVI */
	{ NULL, INVALID_OFFSET },			/* No.97  予約 */
	{ ICU_IER0C_ADDR, ICU_IEN2_BIT },	/* No.98  AD0 ADI0 */
	{ ICU_IER0C_ADDR, ICU_IEN3_BIT },	/* No.99  AD1 ADI1 */
	{ NULL, INVALID_OFFSET },			/* No.100 予約 */
	{ NULL, INVALID_OFFSET },			/* No.101 予約 */
	{ ICU_IER0C_ADDR, ICU_IEN6_BIT },	/* No.102 S12AD0 */
	{ NULL, INVALID_OFFSET },			/* No.103 予約 */
	{ NULL, INVALID_OFFSET },			/* No.104 予約 */
	{ NULL, INVALID_OFFSET },			/* No.105 予約 */
	{ NULL, INVALID_OFFSET },			/* No.106 予約 */
	{ NULL, INVALID_OFFSET },			/* No.107 予約 */
	{ NULL, INVALID_OFFSET },			/* No.108 予約 */
	{ NULL, INVALID_OFFSET },			/* No.109 予約 */
	{ NULL, INVALID_OFFSET },			/* No.110 予約 */
	{ NULL, INVALID_OFFSET },			/* No.111 予約 */
	{ NULL, INVALID_OFFSET },			/* No.112 予約 */
	{ NULL, INVALID_OFFSET },			/* No.113 予約 */
	{ ICU_IER0E_ADDR, ICU_IEN2_BIT },	/* No.114 MTU0 TGIA0 */
	{ ICU_IER0E_ADDR, ICU_IEN3_BIT },	/* No.115 MTU0 TGIB0 */
	{ ICU_IER0E_ADDR, ICU_IEN4_BIT },	/* No.116 MTU0 TGIC0 */
	{ ICU_IER0E_ADDR, ICU_IEN5_BIT },	/* No.117 MTU0 TGID0 */
	{ ICU_IER0E_ADDR, ICU_IEN6_BIT },	/* No.118 MTU0 TCIV0 */
	{ ICU_IER0E_ADDR, ICU_IEN7_BIT },	/* No.119 MTU0 TCIE0 */
	{ ICU_IER0F_ADDR, ICU_IEN0_BIT },	/* No.120 MTU0 TCIF0 */
	{ ICU_IER0F_ADDR, ICU_IEN1_BIT },	/* No.121 MTU1 TGIA1 */
	{ ICU_IER0F_ADDR, ICU_IEN2_BIT },	/* No.122 MTU1 TGIB1 */
	{ ICU_IER0F_ADDR, ICU_IEN3_BIT },	/* No.123 MTU1 TCIV1 */
	{ ICU_IER0F_ADDR, ICU_IEN4_BIT },	/* No.124 MTU1 TCIU1 */
	{ ICU_IER0F_ADDR, ICU_IEN5_BIT },	/* No.125 MTU2 TGIA2 */
	{ ICU_IER0F_ADDR, ICU_IEN6_BIT },	/* No.126 MTU2 TGIB2 */
	{ ICU_IER0F_ADDR, ICU_IEN7_BIT },	/* No.127 MTU2 TCIV2 */
	{ ICU_IER10_ADDR, ICU_IEN0_BIT },	/* No.128 MTU2 TCIU2 */
	{ ICU_IER10_ADDR, ICU_IEN1_BIT },	/* No.129 MTU3 TGIA3 */
	{ ICU_IER10_ADDR, ICU_IEN2_BIT },	/* No.130 MTU3 TGIB3 */
	{ ICU_IER10_ADDR, ICU_IEN3_BIT },	/* No.131 MTU3 TCIC3 */
	{ ICU_IER10_ADDR, ICU_IEN4_BIT },	/* No.132 MTU3 TCID3 */
	{ ICU_IER10_ADDR, ICU_IEN5_BIT },	/* No.133 MTU3 TCIV3 */
	{ ICU_IER10_ADDR, ICU_IEN6_BIT },	/* No.134 MTU4 TGIA4 */
	{ ICU_IER10_ADDR, ICU_IEN7_BIT },	/* No.135 MTU4 TGIB4 */
	{ ICU_IER11_ADDR, ICU_IEN0_BIT },	/* No.136 MTU4 TCIC4 */
	{ ICU_IER11_ADDR, ICU_IEN1_BIT },	/* No.137 MTU4 TCID4 */
	{ ICU_IER11_ADDR, ICU_IEN2_BIT },	/* No.138 MTU4 TCIV4 */
	{ ICU_IER11_ADDR, ICU_IEN3_BIT },	/* No.139 MTU5 TCIU5 */
	{ ICU_IER11_ADDR, ICU_IEN4_BIT },	/* No.140 MTU5 TCIV5 */
	{ ICU_IER11_ADDR, ICU_IEN5_BIT },	/* No.141 MTU5 TCIW5 */
	{ ICU_IER11_ADDR, ICU_IEN6_BIT },	/* No.142 MTU6 TGIA6 */
	{ ICU_IER11_ADDR, ICU_IEN7_BIT },	/* No.143 MTU6 TGIB6 */
	{ ICU_IER12_ADDR, ICU_IEN0_BIT },	/* No.144 MTU6 TGIC6 */
	{ ICU_IER12_ADDR, ICU_IEN1_BIT },	/* No.145 MTU6 TGID6 */
	{ ICU_IER12_ADDR, ICU_IEN2_BIT },	/* No.146 MTU6 TCIV6 */
	{ ICU_IER12_ADDR, ICU_IEN3_BIT },	/* No.147 MTU6 TCIE6 */
	{ ICU_IER12_ADDR, ICU_IEN4_BIT },	/* No.148 MTU6 TCIF6 */
	{ ICU_IER12_ADDR, ICU_IEN5_BIT },	/* No.149 MTU7 TGIA7 */
	{ ICU_IER12_ADDR, ICU_IEN6_BIT },	/* No.150 MTU7 TGIB7 */
	{ ICU_IER12_ADDR, ICU_IEN7_BIT },	/* No.151 MTU7 TCIV7 */
	{ ICU_IER13_ADDR, ICU_IEN0_BIT },	/* No.152 MTU7 TCIU7 */
	{ ICU_IER13_ADDR, ICU_IEN1_BIT },	/* No.153 MTU8 TGIA8 */
	{ ICU_IER13_ADDR, ICU_IEN2_BIT },	/* No.154 MTU8 TGIB8 */
	{ ICU_IER13_ADDR, ICU_IEN3_BIT },	/* No.155 MTU8 TCIV8 */
	{ ICU_IER13_ADDR, ICU_IEN4_BIT },	/* No.156 MTU8 TCIU8 */
	{ ICU_IER13_ADDR, ICU_IEN5_BIT },	/* No.157 MTU9 TGIA9 */
	{ ICU_IER13_ADDR, ICU_IEN6_BIT },	/* No.158 MTU9 TGIB9 */
	{ ICU_IER13_ADDR, ICU_IEN7_BIT },	/* No.159 MTU9 TGIC9 */
	{ ICU_IER14_ADDR, ICU_IEN0_BIT },	/* No.160 MTU9 TGID9 */
	{ ICU_IER14_ADDR, ICU_IEN1_BIT },	/* No.161 MTU9 TCIV9 */
	{ ICU_IER14_ADDR, ICU_IEN2_BIT },	/* No.162 MTU10 TGIA10 */
	{ ICU_IER14_ADDR, ICU_IEN3_BIT },	/* No.163 MTU10 TGIB10 */
	{ ICU_IER14_ADDR, ICU_IEN4_BIT },	/* No.164 MTU10 TGIC10 */
	{ ICU_IER14_ADDR, ICU_IEN5_BIT },	/* No.165 MTU10 TGID10 */
	{ ICU_IER14_ADDR, ICU_IEN6_BIT },	/* No.166 MTU10 TCIV10 */
	{ ICU_IER14_ADDR, ICU_IEN7_BIT },	/* No.167 MTU11 TGIU11 */
	{ ICU_IER15_ADDR, ICU_IEN0_BIT },	/* No.168 MTU11 TGIV11 */
	{ ICU_IER15_ADDR, ICU_IEN1_BIT },	/* No.169 MTU11 TCIW11 */
	{ ICU_IER15_ADDR, ICU_IEN2_BIT },	/* No.170 POE OEI1 */
	{ ICU_IER15_ADDR, ICU_IEN3_BIT },	/* No.171 POE OEI2 */
	{ ICU_IER15_ADDR, ICU_IEN4_BIT },	/* No.172 POE OEI3 */
	{ ICU_IER15_ADDR, ICU_IEN5_BIT },	/* No.173 POE OEI4 */
	{ ICU_IER15_ADDR, ICU_IEN6_BIT },	/* No.174 TMR0 CMIA0 */
	{ ICU_IER15_ADDR, ICU_IEN7_BIT },	/* No.175 TMR0 CMIB0 */
	{ ICU_IER16_ADDR, ICU_IEN0_BIT },	/* No.176 TMR0 OVI0 */
	{ ICU_IER16_ADDR, ICU_IEN1_BIT },	/* No.177 TMR1 CMIA1 */
	{ ICU_IER16_ADDR, ICU_IEN2_BIT },	/* No.178 TMR1 CMIB1 */
	{ ICU_IER16_ADDR, ICU_IEN3_BIT },	/* No.179 TMR1 OVI1 */
	{ ICU_IER16_ADDR, ICU_IEN4_BIT },	/* No.180 TMR2 CMIA2 */
	{ ICU_IER16_ADDR, ICU_IEN5_BIT },	/* No.181 TMR2 CMIB2 */
	{ ICU_IER16_ADDR, ICU_IEN6_BIT },	/* No.182 TMR2 OVI2 */
	{ ICU_IER16_ADDR, ICU_IEN7_BIT },	/* No.183 TMR3 CMIA3 */
	{ ICU_IER17_ADDR, ICU_IEN0_BIT },	/* No.184 TMR3 CMIB3 */
	{ ICU_IER17_ADDR, ICU_IEN1_BIT },	/* No.185 TMR3 OVI3 */
	{ NULL, INVALID_OFFSET },			/* No.186 予約 */
	{ NULL, INVALID_OFFSET },			/* No.187 予約 */
	{ NULL, INVALID_OFFSET },			/* No.188 予約 */
	{ NULL, INVALID_OFFSET },			/* No.189 予約 */
	{ NULL, INVALID_OFFSET },			/* No.190 予約 */
	{ NULL, INVALID_OFFSET },			/* No.191 予約 */
	{ NULL, INVALID_OFFSET },			/* No.192 予約 */
	{ NULL, INVALID_OFFSET },			/* No.193 予約 */
	{ NULL, INVALID_OFFSET },			/* No.194 予約 */
	{ NULL, INVALID_OFFSET },			/* No.195 予約 */
	{ NULL, INVALID_OFFSET },			/* No.196 予約 */
	{ NULL, INVALID_OFFSET },			/* No.197 予約 */
	{ ICU_IER18_ADDR, ICU_IEN6_BIT },	/* No.198 DMAC DMTEND0 */
	{ ICU_IER18_ADDR, ICU_IEN7_BIT },	/* No.199 DMAC DMTEND1 */
	{ ICU_IER19_ADDR, ICU_IEN0_BIT },	/* No.200 DMAC DMTEND2 */
	{ ICU_IER19_ADDR, ICU_IEN1_BIT },	/* No.201 DMAC DMTEND3 */
	{ ICU_IER19_ADDR, ICU_IEN2_BIT },	/* No.202 EXDMAC EXDMACI0 */
	{ ICU_IER19_ADDR, ICU_IEN3_BIT },	/* No.203 EXDMAC EXDMACI1 */
	{ NULL, INVALID_OFFSET },			/* No.204 予約 */
	{ NULL, INVALID_OFFSET },			/* No.205 予約 */
	{ NULL, INVALID_OFFSET },			/* No.206 予約 */
	{ NULL, INVALID_OFFSET },			/* No.207 予約 */
	{ NULL, INVALID_OFFSET },			/* No.208 予約 */
	{ NULL, INVALID_OFFSET },			/* No.209 予約 */
	{ NULL, INVALID_OFFSET },			/* No.210 予約 */
	{ NULL, INVALID_OFFSET },			/* No.211 予約 */
	{ NULL, INVALID_OFFSET },			/* No.212 予約 */
	{ NULL, INVALID_OFFSET },			/* No.213 予約 */
	{ ICU_IER1A_ADDR, ICU_IEN6_BIT },	/* No.214 SCI0 ERI0 */
	{ ICU_IER1A_ADDR, ICU_IEN7_BIT },	/* No.215 SCI0 RXI0 */
	{ ICU_IER1B_ADDR, ICU_IEN0_BIT },	/* No.216 SCI0 TXI0 */
	{ ICU_IER1B_ADDR, ICU_IEN1_BIT },	/* No.217 SCI0 TEI0 */
	{ ICU_IER1B_ADDR, ICU_IEN2_BIT },	/* No.218 SCI1 ERI1 */
	{ ICU_IER1B_ADDR, ICU_IEN3_BIT },	/* No.219 SCI1 RXI1 */
	{ ICU_IER1B_ADDR, ICU_IEN4_BIT },	/* No.220 SCI1 TXI1 */
	{ ICU_IER1B_ADDR, ICU_IEN5_BIT },	/* No.221 SCI1 TEI1 */
	{ ICU_IER1B_ADDR, ICU_IEN6_BIT },	/* No.222 SCI2 ERI2 */
	{ ICU_IER1B_ADDR, ICU_IEN7_BIT },	/* No.223 SCI2 RXI2 */
	{ ICU_IER1C_ADDR, ICU_IEN0_BIT },	/* No.224 SCI2 TXI2 */
	{ ICU_IER1C_ADDR, ICU_IEN1_BIT },	/* No.225 SCI2 TEI2 */
	{ ICU_IER1C_ADDR, ICU_IEN2_BIT },	/* No.226 SCI3 ERI3 */
	{ ICU_IER1C_ADDR, ICU_IEN3_BIT },	/* No.227 SCI3 RXI3 */
	{ ICU_IER1C_ADDR, ICU_IEN4_BIT },	/* No.228 SCI3 TXI3 */
	{ ICU_IER1C_ADDR, ICU_IEN5_BIT },	/* No.229 SCI3 TEI3 */
	{ ICU_IER1C_ADDR, ICU_IEN6_BIT },	/* No.230 SCI4 ERI4 */
	{ ICU_IER1C_ADDR, ICU_IEN7_BIT },	/* No.231 SCI4 RXI4 */
	{ ICU_IER1D_ADDR, ICU_IEN0_BIT },	/* No.232 SCI4 TXI4 */
	{ ICU_IER1D_ADDR, ICU_IEN1_BIT },	/* No.233 SCI4 TEI4 */
	{ ICU_IER1D_ADDR, ICU_IEN2_BIT },	/* No.234 SCI5 ERI5 */
	{ ICU_IER1D_ADDR, ICU_IEN3_BIT },	/* No.235 SCI5 RXI5 */
	{ ICU_IER1D_ADDR, ICU_IEN4_BIT },	/* No.236 SCI5 TXI5 */
	{ ICU_IER1D_ADDR, ICU_IEN5_BIT },	/* No.237 SCI5 TEI5 */
	{ ICU_IER1D_ADDR, ICU_IEN6_BIT },	/* No.238 SCI6 ERI6 */
	{ ICU_IER1D_ADDR, ICU_IEN7_BIT },	/* No.239 SCI6 RXI6 */
	{ ICU_IER1E_ADDR, ICU_IEN0_BIT },	/* No.240 SCI6 TXI6 */
	{ ICU_IER1E_ADDR, ICU_IEN1_BIT },	/* No.241 SCI6 TEI6 */
	{ NULL, INVALID_OFFSET },			/* No.242 予約 */
	{ NULL, INVALID_OFFSET },			/* No.243 予約 */
	{ NULL, INVALID_OFFSET },			/* No.244 予約 */
	{ NULL, INVALID_OFFSET },			/* No.245 予約 */
	{ ICU_IER1E_ADDR, ICU_IEN6_BIT },	/* No.246 RIIC0 ICEEI0 */
	{ ICU_IER1E_ADDR, ICU_IEN7_BIT },	/* No.247 RIIC0 ICRXI0 */
	{ ICU_IER1F_ADDR, ICU_IEN0_BIT },	/* No.248 RIIC0 ICTXI0 */
	{ ICU_IER1F_ADDR, ICU_IEN1_BIT },	/* No.249 RIIC0 ICTEI0 */
	{ ICU_IER1F_ADDR, ICU_IEN2_BIT },	/* No.250 RIIC1 ICEEI1 */
	{ ICU_IER1F_ADDR, ICU_IEN3_BIT },	/* No.251 RIIC1 ICRXI1 */
	{ ICU_IER1F_ADDR, ICU_IEN4_BIT },	/* No.252 RIIC1 ICTXI1 */
	{ ICU_IER1F_ADDR, ICU_IEN5_BIT },	/* No.253 RIIC1 ICTEI1 */
	{ NULL, INVALID_OFFSET },			/* No.254 予約 */
	{ NULL, INVALID_OFFSET },			/* No.255 予約 */
};


/*
 *  IRQコントロールレジスタアドレステーブル
 */
volatile uint8_t __evenaccess * const irqcr_reg_addr[ IRQ_MAX ] = {
	ICU_IRQ0_ADDR,
	ICU_IRQ1_ADDR,
	ICU_IRQ2_ADDR,
	ICU_IRQ3_ADDR,
	ICU_IRQ4_ADDR,
	ICU_IRQ5_ADDR,
	ICU_IRQ6_ADDR,
	ICU_IRQ7_ADDR,
	ICU_IRQ8_ADDR,
	ICU_IRQ9_ADDR,
	ICU_IRQ10_ADDR,
	ICU_IRQ11_ADDR,
	ICU_IRQ12_ADDR,
	ICU_IRQ13_ADDR,
	ICU_IRQ14_ADDR,
	ICU_IRQ15_ADDR,
};

