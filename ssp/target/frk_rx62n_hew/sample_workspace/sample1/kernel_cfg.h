/* kernel_cfg.h */
#ifndef TOPPERS_KERNEL_CFG_H
#define TOPPERS_KERNEL_CFG_H

#define TNUM_TSKID	5
#define TNUM_CYCID	1
#define TNUM_ALMID	0

#define INIT_TASK	1
#define MAIN_TASK	2
#define TASK1	3
#define TASK2	4
#define TASK3	5
#define MAIN_CYC	1
#endif /* TOPPERS_KERNEL_CFG_H */

