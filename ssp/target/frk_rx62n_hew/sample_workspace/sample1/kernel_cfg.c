/* kernel_cfg.c */
#include "kernel/kernel_int.h"
#include "kernel_cfg.h"
#include "time_event.h"

#ifndef TOPPERS_EMPTY_LABEL
#define TOPPERS_EMPTY_LABEL(x,y) x y[0]
#endif

/*
 *  Include Directives (#include)
 */

#include "target_timer.h"
#include "target_syssvc.h"
#include <target_serial.h>
#include "syssvc/serial.h"
#include "syssvc/banner.h"
#include "sample1.h"

/*
 *  Task Management Functions
 */

const ID _kernel_tmax_tskid = (TMIN_TSKID + TNUM_TSKID - 1);

const ATR     	_kernel_tinib_tskatr[TNUM_TSKID]    = {(TA_ACT),(TA_NULL),(TA_NULL),(TA_NULL),(TA_NULL)};
const uint_t	_kernel_init_rdypmap = 1U;
const intptr_t	_kernel_tinib_exinf[TNUM_TSKID]     = {(intptr_t)(0),(intptr_t)(0),(intptr_t)(1),(intptr_t)(2),(intptr_t)(3)};
const TASK    	_kernel_tinib_task[TNUM_TSKID]      = {(init_task),(main_task),(task),(task),(task)};
const uint_t  	_kernel_tinib_epriority[TNUM_TSKID] = {INT_PRIORITY(1),INT_PRIORITY(2),INT_PRIORITY(3),INT_PRIORITY(4),INT_PRIORITY(4)};

/* 
 * Task Stack Size Estimation: 
 * 
 * Call calc_stksz (argv = TASK3, StackSize[TASK3]=STACK_SIZE)
 * 	higher_pri_tsklist[TASK3] = INIT_TASK,MAIN_TASK,TASK1
 * 	Call calc_stksz (argv = INIT_TASK, StackSize[INIT_TASK]=STACK_SIZE)
 * 		higher_pri_tsklist[INIT_TASK] = 
 * 		higher_pri_stkszlist[INIT_TASK] = 0
 * 		DONE(stksz_list[INIT_TASK] = 512)
 * 	Call calc_stksz (argv = MAIN_TASK, StackSize[MAIN_TASK]=STACK_SIZE)
 * 		higher_pri_tsklist[MAIN_TASK] = INIT_TASK
 * 		Call calc_stksz (argv = INIT_TASK, StackSize[INIT_TASK]=STACK_SIZE)
 * 			SKIP(stksz_list[INIT_TASK] = 512)
 * 		higher_pri_stkszlist[MAIN_TASK] = 512
 * 		DONE(stksz_list[MAIN_TASK] = 1024)
 * 	Call calc_stksz (argv = TASK1, StackSize[TASK1]=STACK_SIZE)
 * 		higher_pri_tsklist[TASK1] = INIT_TASK,MAIN_TASK
 * 		Call calc_stksz (argv = INIT_TASK, StackSize[INIT_TASK]=STACK_SIZE)
 * 			SKIP(stksz_list[INIT_TASK] = 512)
 * 		Call calc_stksz (argv = MAIN_TASK, StackSize[MAIN_TASK]=STACK_SIZE)
 * 			SKIP(stksz_list[MAIN_TASK] = 1024)
 * 		higher_pri_stkszlist[TASK1] = 512,1024
 * 		DONE(stksz_list[TASK1] = 1024,1536)
 * 	higher_pri_stkszlist[TASK3] = 512,1024,1024,1536
 * 	DONE(stksz_list[TASK3] = 1024,1536,1536,2048)
 * Call calc_stksz (argv = TASK2, StackSize[TASK2]=STACK_SIZE)
 * 	higher_pri_tsklist[TASK2] = INIT_TASK,MAIN_TASK,TASK1
 * 	Call calc_stksz (argv = INIT_TASK, StackSize[INIT_TASK]=STACK_SIZE)
 * 		SKIP(stksz_list[INIT_TASK] = 512)
 * 	Call calc_stksz (argv = MAIN_TASK, StackSize[MAIN_TASK]=STACK_SIZE)
 * 		SKIP(stksz_list[MAIN_TASK] = 1024)
 * 	Call calc_stksz (argv = TASK1, StackSize[TASK1]=STACK_SIZE)
 * 		SKIP(stksz_list[TASK1] = 1024,1536)
 * 	higher_pri_stkszlist[TASK2] = 512,1024,1024,1536
 * 	DONE(stksz_list[TASK2] = 1024,1536,1536,2048)
 * Estimated Task Stack Size List = 1024,1536,1536,2048,1024,1536,1536,2048
 * Maximum Task Stack Size = 2048
 */ 

/*
 *  Interrupt Management Functions
 */

void
_kernel_inthdr_215(void)
{
	i_begin_int(215);
	((ISR)(sio_rx_isr))((intptr_t)(SIO_PORTID));
	i_end_int(215);
}
void
_kernel_inthdr_217(void)
{
	i_begin_int(217);
	((ISR)(sio_tx_isr))((intptr_t)(SIO_PORTID));
	i_end_int(217);
}
void
_kernel_inthdr_223(void)
{
	i_begin_int(223);
	((ISR)(sio_rx_isr))((intptr_t)(3));
	i_end_int(223);
}
void
_kernel_inthdr_225(void)
{
	i_begin_int(225);
	((ISR)(sio_tx_isr))((intptr_t)(3));
	i_end_int(225);
}

#define TNUM_INHNO	5
const uint_t _kernel_tnum_inhno = TNUM_INHNO;

INTHDR_ENTRY(INHNO_TIMER, 28, target_timer_handler)

INTHDR_ENTRY(215, 215, _kernel_inthdr_215)

INTHDR_ENTRY(217, 217, _kernel_inthdr_217)

INTHDR_ENTRY(223, 223, _kernel_inthdr_223)

INTHDR_ENTRY(225, 225, _kernel_inthdr_225)

const INHNO _kernel_inhinib_inhno[TNUM_INHNO] = {(INHNO_TIMER),(215),(217),(223),(225)};
const ATR _kernel_inhinib_inhatr[TNUM_INHNO] = {(TA_NULL),(TA_NULL),(TA_NULL),(TA_NULL),(TA_NULL)};
const FP _kernel_inhinib_entry[TNUM_INHNO] = {(FP)(INT_ENTRY(INHNO_TIMER, target_timer_handler)),(FP)(INT_ENTRY(215, _kernel_inthdr_215)),(FP)(INT_ENTRY(217, _kernel_inthdr_217)),(FP)(INT_ENTRY(223, _kernel_inthdr_223)),(FP)(INT_ENTRY(225, _kernel_inthdr_225))};

#define TNUM_INTNO	5
const uint_t _kernel_tnum_intno = TNUM_INTNO;

const INTNO _kernel_intinib_intno[TNUM_INTNO] = {(INTNO_TIMER),(INTNO_SIO_TX),(INTNO_SIO_RX),(INT_SCI2_TEI),(INT_SCI2_RXI)};
const ATR _kernel_intinib_intatr[TNUM_INTNO] = {(INTATR_TIMER),(INTATR_SIO),(INTATR_SIO),(INTATR_SIO),(INTATR_SIO)};
const PRI _kernel_intinib_intpri[TNUM_INTNO] = {(INTPRI_TIMER),(INTPRI_SIO),(INTPRI_SIO),(-5),(-5)};

/*
 *  CPU Exception Handler
 */

#define TNUM_EXCNO	1
const uint_t _kernel_tnum_excno = TNUM_EXCNO;

EXCHDR_ENTRY(CPUEXC1, 25, exc_handler)

const EXCNO _kernel_excinib_excno[TNUM_EXCNO] = {(CPUEXC1)};
const ATR _kernel_excinib_excatr[TNUM_EXCNO] = {(TA_NULL)};
const FP _kernel_excinib_entry[TNUM_EXCNO] = {(FP)(EXC_ENTRY(CPUEXC1, exc_handler))};

/*
 *  Cyclic Handler Functions
 */

const ID _kernel_tmax_cycid = (TMIN_CYCID + TNUM_CYCID - 1);
const uint_t _kernel_cycevtid_offset = 0;

const uint_t _kernel_cycinib_cycact = 1;
const intptr_t _kernel_cycinib_exinf[TNUM_CYCID] = {(intptr_t)(MAIN_TASK)};
const CYCHDR _kernel_cycinib_cychdr[TNUM_CYCID] = {(cyclic_handler)};
const RELTIM _kernel_cycinib_cyctim[TNUM_CYCID] = {(2000)};
const RELTIM _kernel_cycinib_cycphs[TNUM_CYCID] = {(1000)};

uint_t _kernel_cyccb_cycact;
EVTTIM _kernel_cyccb_evttim[TNUM_CYCID];


/*
 *  Alarm Handler Functions
 */

const ID _kernel_tmax_almid = (TMIN_ALMID + TNUM_ALMID - 1);
const uint_t _kernel_almevtid_offset = 1;

TOPPERS_EMPTY_LABEL(const ALMHDR, _kernel_alminib_almhdr);
TOPPERS_EMPTY_LABEL(const intptr_t, _kernel_alminib_exinf);

uint_t _kernel_almcb_almact;

/*
 *  Time Event Management
 */

#define TNUM_TMEVT 1

const uint_t _kernel_tnum_tmevt_queue = 1;

QUEUE _kernel_tmevt_queue[TNUM_TMEVT+1];
EVTTIM _kernel_tmevt_time[TNUM_TMEVT];
CBACK _kernel_tmevt_callback[TNUM_TMEVT];
uintptr_t _kernel_tmevt_arg[TNUM_TMEVT];


/*
 *  Stack Area for System
 */

#define TOPPERS_ISTKSZ		DEFAULT_ISTKSZ
static STK_T          		_kernel_stack[COUNT_STK_T(TOPPERS_ISTKSZ)];
#define TOPPERS_STK   		_kernel_stack
#define TOPPERS_STKSZ		ROUND_STK_T(TOPPERS_ISTKSZ)


const SIZE		_kernel_stksz = TOPPERS_STKSZ;
STK_T *const	_kernel_stk = TOPPERS_STK;

#ifdef TOPPERS_ISTKPT
STK_T *const	_kernel_istkpt = TOPPERS_ISTKPT(TOPPERS_STK, TOPPERS_STKSZ);
#endif /* TOPPERS_ISTKPT */

/*
 *  Module Initialization Function
 */

void
_kernel_initialize_object(void)
{
	_kernel_initialize_time_event();
	_kernel_initialize_task();
	_kernel_initialize_interrupt();
	_kernel_initialize_exception();
	_kernel_initialize_cyclic();
}

/*
 *  Initialization Routine
 */

void
_kernel_call_inirtn(void)
{
	((INIRTN)(target_timer_initialize))((intptr_t)(0));
	((INIRTN)(sio_initialize))((intptr_t)(0));
	((INIRTN)(serial_initialize))((intptr_t)(0));
	((INIRTN)(print_banner))((intptr_t)(0));
}

/*
 *  Termination Routine
 */

void
_kernel_call_terrtn(void)
{
	((TERRTN)(serial_terminate))((intptr_t)(0));
	((TERRTN)(target_timer_terminate))((intptr_t)(0));
}

/*
 *  Interrupt Level and Attribute Table
 */
const CFG_INT_INFO _kernel_cfg_int_table[256] = {
	{  0, 0xFFFFFFFF }, /* 00 */
	{  0, 0xFFFFFFFF }, /* 01 */
	{  0, 0xFFFFFFFF }, /* 02 */
	{  0, 0xFFFFFFFF }, /* 03 */
	{  0, 0xFFFFFFFF }, /* 04 */
	{  0, 0xFFFFFFFF }, /* 05 */
	{  0, 0xFFFFFFFF }, /* 06 */
	{  0, 0xFFFFFFFF }, /* 07 */
	{  0, 0xFFFFFFFF }, /* 08 */
	{  0, 0xFFFFFFFF }, /* 09 */
	{  0, 0xFFFFFFFF }, /* 10 */
	{  0, 0xFFFFFFFF }, /* 11 */
	{  0, 0xFFFFFFFF }, /* 12 */
	{  0, 0xFFFFFFFF }, /* 13 */
	{  0, 0xFFFFFFFF }, /* 14 */
	{  0, 0xFFFFFFFF }, /* 15 */
	{  0, 0xFFFFFFFF }, /* 16 */
	{  0, 0xFFFFFFFF }, /* 17 */
	{  0, 0xFFFFFFFF }, /* 18 */
	{  0, 0xFFFFFFFF }, /* 19 */
	{  0, 0xFFFFFFFF }, /* 20 */
	{  0, 0xFFFFFFFF }, /* 21 */
	{  0, 0xFFFFFFFF }, /* 22 */
	{  0, 0xFFFFFFFF }, /* 23 */
	{  0, 0xFFFFFFFF }, /* 24 */
	{  0, 0xFFFFFFFF }, /* 25 */
	{  0, 0xFFFFFFFF }, /* 26 */
	{  0, 0xFFFFFFFF }, /* 27 */
	{  5, INTATR_TIMER }, /* 28 */
	{  0, 0xFFFFFFFF }, /* 29 */
	{  0, 0xFFFFFFFF }, /* 30 */
	{  0, 0xFFFFFFFF }, /* 31 */
	{  0, 0xFFFFFFFF }, /* 32 */
	{  0, 0xFFFFFFFF }, /* 33 */
	{  0, 0xFFFFFFFF }, /* 34 */
	{  0, 0xFFFFFFFF }, /* 35 */
	{  0, 0xFFFFFFFF }, /* 36 */
	{  0, 0xFFFFFFFF }, /* 37 */
	{  0, 0xFFFFFFFF }, /* 38 */
	{  0, 0xFFFFFFFF }, /* 39 */
	{  0, 0xFFFFFFFF }, /* 40 */
	{  0, 0xFFFFFFFF }, /* 41 */
	{  0, 0xFFFFFFFF }, /* 42 */
	{  0, 0xFFFFFFFF }, /* 43 */
	{  0, 0xFFFFFFFF }, /* 44 */
	{  0, 0xFFFFFFFF }, /* 45 */
	{  0, 0xFFFFFFFF }, /* 46 */
	{  0, 0xFFFFFFFF }, /* 47 */
	{  0, 0xFFFFFFFF }, /* 48 */
	{  0, 0xFFFFFFFF }, /* 49 */
	{  0, 0xFFFFFFFF }, /* 50 */
	{  0, 0xFFFFFFFF }, /* 51 */
	{  0, 0xFFFFFFFF }, /* 52 */
	{  0, 0xFFFFFFFF }, /* 53 */
	{  0, 0xFFFFFFFF }, /* 54 */
	{  0, 0xFFFFFFFF }, /* 55 */
	{  0, 0xFFFFFFFF }, /* 56 */
	{  0, 0xFFFFFFFF }, /* 57 */
	{  0, 0xFFFFFFFF }, /* 58 */
	{  0, 0xFFFFFFFF }, /* 59 */
	{  0, 0xFFFFFFFF }, /* 60 */
	{  0, 0xFFFFFFFF }, /* 61 */
	{  0, 0xFFFFFFFF }, /* 62 */
	{  0, 0xFFFFFFFF }, /* 63 */
	{  0, 0xFFFFFFFF }, /* 64 */
	{  0, 0xFFFFFFFF }, /* 65 */
	{  0, 0xFFFFFFFF }, /* 66 */
	{  0, 0xFFFFFFFF }, /* 67 */
	{  0, 0xFFFFFFFF }, /* 68 */
	{  0, 0xFFFFFFFF }, /* 69 */
	{  0, 0xFFFFFFFF }, /* 70 */
	{  0, 0xFFFFFFFF }, /* 71 */
	{  0, 0xFFFFFFFF }, /* 72 */
	{  0, 0xFFFFFFFF }, /* 73 */
	{  0, 0xFFFFFFFF }, /* 74 */
	{  0, 0xFFFFFFFF }, /* 75 */
	{  0, 0xFFFFFFFF }, /* 76 */
	{  0, 0xFFFFFFFF }, /* 77 */
	{  0, 0xFFFFFFFF }, /* 78 */
	{  0, 0xFFFFFFFF }, /* 79 */
	{  0, 0xFFFFFFFF }, /* 80 */
	{  0, 0xFFFFFFFF }, /* 81 */
	{  0, 0xFFFFFFFF }, /* 82 */
	{  0, 0xFFFFFFFF }, /* 83 */
	{  0, 0xFFFFFFFF }, /* 84 */
	{  0, 0xFFFFFFFF }, /* 85 */
	{  0, 0xFFFFFFFF }, /* 86 */
	{  0, 0xFFFFFFFF }, /* 87 */
	{  0, 0xFFFFFFFF }, /* 88 */
	{  0, 0xFFFFFFFF }, /* 89 */
	{  0, 0xFFFFFFFF }, /* 90 */
	{  0, 0xFFFFFFFF }, /* 91 */
	{  0, 0xFFFFFFFF }, /* 92 */
	{  0, 0xFFFFFFFF }, /* 93 */
	{  0, 0xFFFFFFFF }, /* 94 */
	{  0, 0xFFFFFFFF }, /* 95 */
	{  0, 0xFFFFFFFF }, /* 96 */
	{  0, 0xFFFFFFFF }, /* 97 */
	{  0, 0xFFFFFFFF }, /* 98 */
	{  0, 0xFFFFFFFF }, /* 99 */
	{  0, 0xFFFFFFFF }, /* 100 */
	{  0, 0xFFFFFFFF }, /* 101 */
	{  0, 0xFFFFFFFF }, /* 102 */
	{  0, 0xFFFFFFFF }, /* 103 */
	{  0, 0xFFFFFFFF }, /* 104 */
	{  0, 0xFFFFFFFF }, /* 105 */
	{  0, 0xFFFFFFFF }, /* 106 */
	{  0, 0xFFFFFFFF }, /* 107 */
	{  0, 0xFFFFFFFF }, /* 108 */
	{  0, 0xFFFFFFFF }, /* 109 */
	{  0, 0xFFFFFFFF }, /* 110 */
	{  0, 0xFFFFFFFF }, /* 111 */
	{  0, 0xFFFFFFFF }, /* 112 */
	{  0, 0xFFFFFFFF }, /* 113 */
	{  0, 0xFFFFFFFF }, /* 114 */
	{  0, 0xFFFFFFFF }, /* 115 */
	{  0, 0xFFFFFFFF }, /* 116 */
	{  0, 0xFFFFFFFF }, /* 117 */
	{  0, 0xFFFFFFFF }, /* 118 */
	{  0, 0xFFFFFFFF }, /* 119 */
	{  0, 0xFFFFFFFF }, /* 120 */
	{  0, 0xFFFFFFFF }, /* 121 */
	{  0, 0xFFFFFFFF }, /* 122 */
	{  0, 0xFFFFFFFF }, /* 123 */
	{  0, 0xFFFFFFFF }, /* 124 */
	{  0, 0xFFFFFFFF }, /* 125 */
	{  0, 0xFFFFFFFF }, /* 126 */
	{  0, 0xFFFFFFFF }, /* 127 */
	{  0, 0xFFFFFFFF }, /* 128 */
	{  0, 0xFFFFFFFF }, /* 129 */
	{  0, 0xFFFFFFFF }, /* 130 */
	{  0, 0xFFFFFFFF }, /* 131 */
	{  0, 0xFFFFFFFF }, /* 132 */
	{  0, 0xFFFFFFFF }, /* 133 */
	{  0, 0xFFFFFFFF }, /* 134 */
	{  0, 0xFFFFFFFF }, /* 135 */
	{  0, 0xFFFFFFFF }, /* 136 */
	{  0, 0xFFFFFFFF }, /* 137 */
	{  0, 0xFFFFFFFF }, /* 138 */
	{  0, 0xFFFFFFFF }, /* 139 */
	{  0, 0xFFFFFFFF }, /* 140 */
	{  0, 0xFFFFFFFF }, /* 141 */
	{  0, 0xFFFFFFFF }, /* 142 */
	{  0, 0xFFFFFFFF }, /* 143 */
	{  0, 0xFFFFFFFF }, /* 144 */
	{  0, 0xFFFFFFFF }, /* 145 */
	{  0, 0xFFFFFFFF }, /* 146 */
	{  0, 0xFFFFFFFF }, /* 147 */
	{  0, 0xFFFFFFFF }, /* 148 */
	{  0, 0xFFFFFFFF }, /* 149 */
	{  0, 0xFFFFFFFF }, /* 150 */
	{  0, 0xFFFFFFFF }, /* 151 */
	{  0, 0xFFFFFFFF }, /* 152 */
	{  0, 0xFFFFFFFF }, /* 153 */
	{  0, 0xFFFFFFFF }, /* 154 */
	{  0, 0xFFFFFFFF }, /* 155 */
	{  0, 0xFFFFFFFF }, /* 156 */
	{  0, 0xFFFFFFFF }, /* 157 */
	{  0, 0xFFFFFFFF }, /* 158 */
	{  0, 0xFFFFFFFF }, /* 159 */
	{  0, 0xFFFFFFFF }, /* 160 */
	{  0, 0xFFFFFFFF }, /* 161 */
	{  0, 0xFFFFFFFF }, /* 162 */
	{  0, 0xFFFFFFFF }, /* 163 */
	{  0, 0xFFFFFFFF }, /* 164 */
	{  0, 0xFFFFFFFF }, /* 165 */
	{  0, 0xFFFFFFFF }, /* 166 */
	{  0, 0xFFFFFFFF }, /* 167 */
	{  0, 0xFFFFFFFF }, /* 168 */
	{  0, 0xFFFFFFFF }, /* 169 */
	{  0, 0xFFFFFFFF }, /* 170 */
	{  0, 0xFFFFFFFF }, /* 171 */
	{  0, 0xFFFFFFFF }, /* 172 */
	{  0, 0xFFFFFFFF }, /* 173 */
	{  0, 0xFFFFFFFF }, /* 174 */
	{  0, 0xFFFFFFFF }, /* 175 */
	{  0, 0xFFFFFFFF }, /* 176 */
	{  0, 0xFFFFFFFF }, /* 177 */
	{  0, 0xFFFFFFFF }, /* 178 */
	{  0, 0xFFFFFFFF }, /* 179 */
	{  0, 0xFFFFFFFF }, /* 180 */
	{  0, 0xFFFFFFFF }, /* 181 */
	{  0, 0xFFFFFFFF }, /* 182 */
	{  0, 0xFFFFFFFF }, /* 183 */
	{  0, 0xFFFFFFFF }, /* 184 */
	{  0, 0xFFFFFFFF }, /* 185 */
	{  0, 0xFFFFFFFF }, /* 186 */
	{  0, 0xFFFFFFFF }, /* 187 */
	{  0, 0xFFFFFFFF }, /* 188 */
	{  0, 0xFFFFFFFF }, /* 189 */
	{  0, 0xFFFFFFFF }, /* 190 */
	{  0, 0xFFFFFFFF }, /* 191 */
	{  0, 0xFFFFFFFF }, /* 192 */
	{  0, 0xFFFFFFFF }, /* 193 */
	{  0, 0xFFFFFFFF }, /* 194 */
	{  0, 0xFFFFFFFF }, /* 195 */
	{  0, 0xFFFFFFFF }, /* 196 */
	{  0, 0xFFFFFFFF }, /* 197 */
	{  0, 0xFFFFFFFF }, /* 198 */
	{  0, 0xFFFFFFFF }, /* 199 */
	{  0, 0xFFFFFFFF }, /* 200 */
	{  0, 0xFFFFFFFF }, /* 201 */
	{  0, 0xFFFFFFFF }, /* 202 */
	{  0, 0xFFFFFFFF }, /* 203 */
	{  0, 0xFFFFFFFF }, /* 204 */
	{  0, 0xFFFFFFFF }, /* 205 */
	{  0, 0xFFFFFFFF }, /* 206 */
	{  0, 0xFFFFFFFF }, /* 207 */
	{  0, 0xFFFFFFFF }, /* 208 */
	{  0, 0xFFFFFFFF }, /* 209 */
	{  0, 0xFFFFFFFF }, /* 210 */
	{  0, 0xFFFFFFFF }, /* 211 */
	{  0, 0xFFFFFFFF }, /* 212 */
	{  0, 0xFFFFFFFF }, /* 213 */
	{  0, 0xFFFFFFFF }, /* 214 */
	{  4, INTATR_SIO }, /* 215 */
	{  0, 0xFFFFFFFF }, /* 216 */
	{  4, INTATR_SIO }, /* 217 */
	{  0, 0xFFFFFFFF }, /* 218 */
	{  0, 0xFFFFFFFF }, /* 219 */
	{  0, 0xFFFFFFFF }, /* 220 */
	{  0, 0xFFFFFFFF }, /* 221 */
	{  0, 0xFFFFFFFF }, /* 222 */
	{  5, INTATR_SIO }, /* 223 */
	{  0, 0xFFFFFFFF }, /* 224 */
	{  5, INTATR_SIO }, /* 225 */
	{  0, 0xFFFFFFFF }, /* 226 */
	{  0, 0xFFFFFFFF }, /* 227 */
	{  0, 0xFFFFFFFF }, /* 228 */
	{  0, 0xFFFFFFFF }, /* 229 */
	{  0, 0xFFFFFFFF }, /* 230 */
	{  0, 0xFFFFFFFF }, /* 231 */
	{  0, 0xFFFFFFFF }, /* 232 */
	{  0, 0xFFFFFFFF }, /* 233 */
	{  0, 0xFFFFFFFF }, /* 234 */
	{  0, 0xFFFFFFFF }, /* 235 */
	{  0, 0xFFFFFFFF }, /* 236 */
	{  0, 0xFFFFFFFF }, /* 237 */
	{  0, 0xFFFFFFFF }, /* 238 */
	{  0, 0xFFFFFFFF }, /* 239 */
	{  0, 0xFFFFFFFF }, /* 240 */
	{  0, 0xFFFFFFFF }, /* 241 */
	{  0, 0xFFFFFFFF }, /* 242 */
	{  0, 0xFFFFFFFF }, /* 243 */
	{  0, 0xFFFFFFFF }, /* 244 */
	{  0, 0xFFFFFFFF }, /* 245 */
	{  0, 0xFFFFFFFF }, /* 246 */
	{  0, 0xFFFFFFFF }, /* 247 */
	{  0, 0xFFFFFFFF }, /* 248 */
	{  0, 0xFFFFFFFF }, /* 249 */
	{  0, 0xFFFFFFFF }, /* 250 */
	{  0, 0xFFFFFFFF }, /* 251 */
	{  0, 0xFFFFFFFF }, /* 252 */
	{  0, 0xFFFFFFFF }, /* 253 */
	{  0, 0xFFFFFFFF }, /* 254 */
	{  0, 0xFFFFFFFF }, /* 255 */
};




#ifndef TARGET_COPYRIGHT
#define TARGET_COPYRIGHT
#endif /* TARGET_COPYRIGHT */


const char banner[] = "\n" "TOPPERS/SSP Kernel " "1.10.0 " 	\
		" for " TARGET_NAME " (" __DATE__ " , " __TIME__ ")\n"	\
		"Copyright (C) 2010 by Meika Sugimoto\n"	\
		"Copyright (C) 2010 by Naoki Saito\n"	\
		"            Nagoya Municipal Industrial Research Institute, JAPAN\n"	\
		TARGET_COPYRIGHT "\n";
const int32_t banner_size = sizeof(banner) / sizeof(banner[0]);

