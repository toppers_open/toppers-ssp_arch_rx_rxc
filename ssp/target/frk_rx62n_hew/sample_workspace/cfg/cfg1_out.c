/* cfg1_out.c */
#define TOPPERS_CFG1_OUT  1
#include "kernel/kernel_int.h"
#include "target_timer.h"
#include "target_syssvc.h"
#include <target_serial.h>
#include "syssvc/serial.h"
#include "syssvc/banner.h"
#include "sample1.h"


#ifdef INT64_MAX
  typedef int64_t signed_t;
  typedef uint64_t unsigned_t;
#else
  typedef int32_t signed_t;
  typedef uint32_t unsigned_t;
#endif

#include <target_cfg1_out.h>

const uint32_t TOPPERS_cfg_magic_number = 0x12345678;
const uint32_t TOPPERS_cfg_sizeof_signed_t = sizeof(signed_t);
const unsigned_t TOPPERS_cfg_CHAR_BIT = CHAR_BIT;
const unsigned_t TOPPERS_cfg_CHAR_MAX = CHAR_MAX;
const unsigned_t TOPPERS_cfg_CHAR_MIN = CHAR_MIN;
const unsigned_t TOPPERS_cfg_SCHAR_MAX = SCHAR_MAX;
const unsigned_t TOPPERS_cfg_SHRT_MAX = SHRT_MAX;
const unsigned_t TOPPERS_cfg_INT_MAX = INT_MAX;
const unsigned_t TOPPERS_cfg_LONG_MAX = LONG_MAX;

const unsigned_t TOPPERS_cfg_TA_NULL = TA_NULL;
const unsigned_t TOPPERS_cfg_TA_ACT = TA_ACT;
const unsigned_t TOPPERS_cfg_TA_RSTR = TA_RSTR;
const unsigned_t TOPPERS_cfg_TA_STA = TA_STA;
const unsigned_t TOPPERS_cfg_TA_NONKERNEL = TA_NONKERNEL;
const unsigned_t TOPPERS_cfg_TA_ENAINT = TA_ENAINT;
const unsigned_t TOPPERS_cfg_TA_EDGE = TA_EDGE;
const unsigned_t TOPPERS_cfg_DEFAULT_ISTKSZ = DEFAULT_ISTKSZ;
const signed_t TOPPERS_cfg_TMIN_TPRI = TMIN_TPRI;
const signed_t TOPPERS_cfg_TMAX_TPRI = TMAX_TPRI;
const signed_t TOPPERS_cfg_TMIN_ISRPRI = TMIN_ISRPRI;
const signed_t TOPPERS_cfg_TMAX_ISRPRI = TMAX_ISRPRI;
const unsigned_t TOPPERS_cfg_TMAX_RELTIM = TMAX_RELTIM;
const signed_t TOPPERS_cfg_TMIN_INTPRI = TMIN_INTPRI;
const unsigned_t TOPPERS_cfg_OMIT_INITIALIZE_INTERRUPT = 
#if defined(OMIT_INITIALIZE_INTERRUPT)
1;
#else
0;
#endif
const unsigned_t TOPPERS_cfg_OMIT_INITIALIZE_EXCEPTION = 
#if defined(OMIT_INITIALIZE_EXCEPTION)
1;
#else
0;
#endif
const unsigned_t TOPPERS_cfg_sizeof_ID = sizeof(ID);
const unsigned_t TOPPERS_cfg_sizeof_uint_t = sizeof(uint_t);
const unsigned_t TOPPERS_cfg_sizeof_SIZE = sizeof(SIZE);
const unsigned_t TOPPERS_cfg_sizeof_ATR = sizeof(ATR);
const unsigned_t TOPPERS_cfg_sizeof_PRI = sizeof(PRI);
const unsigned_t TOPPERS_cfg_sizeof_void_ptr = sizeof(void*);
const unsigned_t TOPPERS_cfg_sizeof_VP = sizeof(void*);
const unsigned_t TOPPERS_cfg_sizeof_intptr_t = sizeof(intptr_t);
const unsigned_t TOPPERS_cfg_sizeof_FP = sizeof(FP);
const unsigned_t TOPPERS_cfg_sizeof_INHNO = sizeof(INHNO);
const unsigned_t TOPPERS_cfg_sizeof_INTNO = sizeof(INTNO);
const unsigned_t TOPPERS_cfg_sizeof_EXCNO = sizeof(EXCNO);
const unsigned_t TOPPERS_cfg_TA_LOWLEVEL = TA_LOWLEVEL;
const unsigned_t TOPPERS_cfg_TA_BOTHEDGE = TA_BOTHEDGE;
const unsigned_t TOPPERS_cfg_TA_NEGEDGE = TA_NEGEDGE;
const signed_t TOPPERS_cfg_TIPM_LOCK = TIPM_LOCK;
const unsigned_t TOPPERS_cfg_IPL_LOCK = IPL_LOCK;
const unsigned_t TOPPERS_cfg_PSW_I_MASK = PSW_I_MASK;
const unsigned_t TOPPERS_cfg_PSW_IPL_MASK = PSW_IPL_MASK;
const unsigned_t TOPPERS_cfg_EXC_GET_PSW_OFFSET = EXC_GET_PSW_OFFSET;
const unsigned_t TOPPERS_cfg_TKERNEL_PRVER = TKERNEL_PRVER;
const unsigned_t TOPPERS_cfg_LOG_DSP_ENTER = LOG_DSP_ENTER;
const unsigned_t TOPPERS_cfg_LOG_DSP_LEAVE = LOG_DSP_LEAVE;
const unsigned_t TOPPERS_cfg_LOG_INH_ENTER = LOG_INH_ENTER;
const unsigned_t TOPPERS_cfg_LOG_INH_LEAVE = LOG_INH_LEAVE;
const unsigned_t TOPPERS_cfg_LOG_EXC_ENTER = LOG_EXC_ENTER;
const unsigned_t TOPPERS_cfg_LOG_EXC_LEAVE = LOG_EXC_LEAVE;
const unsigned_t TOPPERS_cfg_SIL_DLY_TIM1 = SIL_DLY_TIM1;
const unsigned_t TOPPERS_cfg_SIL_DLY_TIM2 = SIL_DLY_TIM2;



#ifdef USE_TIMER
/* #include "target_timer.h" */

#line 6 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_0 = 0;
const unsigned_t TOPPERS_cfg_valueof_iniatr_0 = ( TA_NULL ); 
#line 7 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_1 = 1;
const unsigned_t TOPPERS_cfg_valueof_teratr_1 = ( TA_NULL ); 
#line 8 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_2 = 2;
const unsigned_t TOPPERS_cfg_valueof_inhno_2 = ( INHNO_TIMER ); const unsigned_t TOPPERS_cfg_valueof_inhatr_2 = ( TA_NULL ); 
#line 9 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_timer.cfg"
const unsigned_t TOPPERS_cfg_static_api_3 = 3;
const unsigned_t TOPPERS_cfg_valueof_intno_3 = ( INTNO_TIMER ); const unsigned_t TOPPERS_cfg_valueof_intatr_3 = ( INTATR_TIMER ); const signed_t TOPPERS_cfg_valueof_intpri_3 = ( INTPRI_TIMER ); 
#endif 
/* #include "target_syssvc.h" */
/* #include <target_serial.h> */

#line 6 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_4 = 4;
const unsigned_t TOPPERS_cfg_valueof_iniatr_4 = ( TA_NULL ); 
#line 7 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_5 = 5;
const unsigned_t TOPPERS_cfg_valueof_intno_5 = ( INTNO_SIO_TX ); const unsigned_t TOPPERS_cfg_valueof_intatr_5 = ( INTATR_SIO ); const signed_t TOPPERS_cfg_valueof_intpri_5 = ( INTPRI_SIO ); 
#line 8 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_6 = 6;
const unsigned_t TOPPERS_cfg_valueof_intno_6 = ( INTNO_SIO_RX ); const unsigned_t TOPPERS_cfg_valueof_intatr_6 = ( INTATR_SIO ); const signed_t TOPPERS_cfg_valueof_intpri_6 = ( INTPRI_SIO ); 
#line 10 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_7 = 7;
const unsigned_t TOPPERS_cfg_valueof_isratr_7 = ( TA_NULL ); const unsigned_t TOPPERS_cfg_valueof_intno_7 = ( INTNO_SIO_TX ); const signed_t TOPPERS_cfg_valueof_isrpri_7 = ( 1 ); 
#line 11 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_8 = 8;
const unsigned_t TOPPERS_cfg_valueof_isratr_8 = ( TA_NULL ); const unsigned_t TOPPERS_cfg_valueof_intno_8 = ( INTNO_SIO_RX ); const signed_t TOPPERS_cfg_valueof_isrpri_8 = ( 1 ); 
#line 13 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_9 = 9;
const unsigned_t TOPPERS_cfg_valueof_intno_9 = ( INT_SCI2_TEI ); const unsigned_t TOPPERS_cfg_valueof_intatr_9 = ( INTATR_SIO ); const signed_t TOPPERS_cfg_valueof_intpri_9 = ( -5 ); 
#line 14 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_10 = 10;
const unsigned_t TOPPERS_cfg_valueof_intno_10 = ( INT_SCI2_RXI ); const unsigned_t TOPPERS_cfg_valueof_intatr_10 = ( INTATR_SIO ); const signed_t TOPPERS_cfg_valueof_intpri_10 = ( -5 ); 
#line 16 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_11 = 11;
const unsigned_t TOPPERS_cfg_valueof_isratr_11 = ( TA_NULL ); const unsigned_t TOPPERS_cfg_valueof_intno_11 = ( INT_SCI2_TEI ); const signed_t TOPPERS_cfg_valueof_isrpri_11 = ( 1 ); 
#line 17 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../target/frk_rx62n_hew/target_serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_12 = 12;
const unsigned_t TOPPERS_cfg_valueof_isratr_12 = ( TA_NULL ); const unsigned_t TOPPERS_cfg_valueof_intno_12 = ( INT_SCI2_RXI ); const signed_t TOPPERS_cfg_valueof_isrpri_12 = ( 1 ); /* #include "syssvc/serial.h" */

#line 13 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../syssvc/serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_13 = 13;
const unsigned_t TOPPERS_cfg_valueof_iniatr_13 = ( TA_NULL ); 
#line 14 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../syssvc/serial.cfg"
const unsigned_t TOPPERS_cfg_static_api_14 = 14;
const unsigned_t TOPPERS_cfg_valueof_teratr_14 = ( TA_NULL ); /* #include "syssvc/banner.h" */

#line 10 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../syssvc/banner.cfg"
const unsigned_t TOPPERS_cfg_static_api_15 = 15;
const unsigned_t TOPPERS_cfg_valueof_iniatr_15 = ( TA_NULL ); /* #include "sample1.h" */

#line 13 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_static_api_16 = 16;
#define INIT_TASK	(<>)

#line 13 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_16 = ( TA_ACT ); const signed_t TOPPERS_cfg_valueof_atskpri_16 = ( INIT_PRIORITY ); const unsigned_t TOPPERS_cfg_valueof_stksz_16 = ( STACK_SIZE ); 
#line 14 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_static_api_17 = 17;
#define MAIN_TASK	(<>)

#line 14 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_17 = ( TA_NULL ); const signed_t TOPPERS_cfg_valueof_atskpri_17 = ( MAIN_PRIORITY ); const unsigned_t TOPPERS_cfg_valueof_stksz_17 = ( STACK_SIZE ); 
#line 15 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_static_api_18 = 18;
#define TASK1	(<>)

#line 15 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_18 = ( TA_NULL ); const signed_t TOPPERS_cfg_valueof_atskpri_18 = ( TASK1_PRIORITY ); const unsigned_t TOPPERS_cfg_valueof_stksz_18 = ( STACK_SIZE ); 
#line 16 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_static_api_19 = 19;
#define TASK2	(<>)

#line 16 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_19 = ( TA_NULL ); const signed_t TOPPERS_cfg_valueof_atskpri_19 = ( TASK2_PRIORITY ); const unsigned_t TOPPERS_cfg_valueof_stksz_19 = ( STACK_SIZE ); 
#line 17 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_static_api_20 = 20;
#define TASK3	(<>)

#line 17 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_valueof_tskatr_20 = ( TA_NULL ); const signed_t TOPPERS_cfg_valueof_atskpri_20 = ( TASK3_PRIORITY ); const unsigned_t TOPPERS_cfg_valueof_stksz_20 = ( STACK_SIZE ); 
#line 19 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_static_api_21 = 21;
const signed_t TOPPERS_cfg_valueof_etskpri_21 = ( TASK3_EXEPRIORITY ); 
#ifdef USE_TIMER

#line 22 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_static_api_22 = 22;
#define MAIN_CYC	(<>)

#line 22 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_valueof_cycatr_22 = ( TA_STA ); const unsigned_t TOPPERS_cfg_valueof_cyctim_22 = ( 2000 ); const unsigned_t TOPPERS_cfg_valueof_cycphs_22 = ( 1000 ); 
#endif 

#ifdef TEST_ISR

#line 26 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_static_api_23 = 23;
const unsigned_t TOPPERS_cfg_valueof_isratr_23 = ( TA_NULL ); const unsigned_t TOPPERS_cfg_valueof_intno_23 = ( ISR1 ); const signed_t TOPPERS_cfg_valueof_isrpri_23 = ( 1 ); 
#endif 

#ifdef TEST_EXC

#line 30 "E:/toppers-ssp/toppers_svn/branches/1.1.0/target/frk_rx62n_hew/sample_workspace/../../../sample/sample1.cfg"
const unsigned_t TOPPERS_cfg_static_api_24 = 24;
const unsigned_t TOPPERS_cfg_valueof_excno_24 = ( CPUEXC1 ); const unsigned_t TOPPERS_cfg_valueof_excatr_24 = ( TA_NULL ); 
#endif 

